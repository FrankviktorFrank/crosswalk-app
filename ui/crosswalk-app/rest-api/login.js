const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const path = require("path");
const con = require("./settings");
const app = express();

app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (request, response) => {
  response.sendFile(path.join(__dirname + "/login.html"));
});

app.post("/api/login", (request, response) => {
  var username = request.body.username;
  var password = request.body.password;
  if (username && password) {
    con.query(
      "SELECT * FROM users WHERE mailUserName = ? AND password = ?",
      [username, password],
      function(error, results, fields) {
        console.log("Yes");
        if (results.length > 0) {
          request.session.loggedin = true;
          request.session.username = username;
          response.send("OK!!");
        } else {
          response.send("Incorrect Username and/or Password!");
        }
        response.end();
      }
    );
  } else {
    response.send(console.log(request.body.username));
    response.end();
  }
});

app.get("/api/home", (request, response) => {
  if (request.session.loggedin) {
    response.send("Welcome back, " + request.session.username + "!");
  } else {
    response.send("Please login to view this page!");
  }
  response.end();
});

app.get("/api/translate/:from/:to/:string/", (request, response) => {
  const from = request.params.from;
  const to = request.params.to;
  const string = request.params.string;

  con.query(
    "SELECT wordId FROM words JOIN language ON language.id = words.languageId WHERE language.name = ? AND word = ?",
    [from, string],
    function(error, results, fields) {
      if (error) {
        console.log(error);
        return;
      } else {
        con.query(
          "SELECT word FROM words JOIN language ON language.id = words.languageId WHERE wordId = ? AND language.name = ?",
          [results[0].wordId, to],
          function(error, results, fields) {
            if (error) {
              console.log(error);
              return;
            } else {
              response.json({ translation: results });
            }
          }
        );
      }
    }
  );
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server listening on port ${PORT} ...`));
