import React, { Component } from "react";
import ProjectItem from "./projectItem";

class LoggedIn extends Component {
  //Kolla om användaren är inloggad. Om sant kör render().
  /*  checkIfLoggedIn = () => {}; */

  render() {
    let projectItems;
    if (this.props.projects) {
      projectItems = this.props.projects.map(item => {
        return <ProjectItem key={item.title} project={item} />;
      });
    }

    return (
      <div className="Projects">
        <h1>Hej och välkommen till förstasidan.</h1>
        {projectItems}
      </div>
    );
  }
}

export default LoggedIn;
