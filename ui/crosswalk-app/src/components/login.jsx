import React, { Component } from "react";
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  Container
} from "react-bootstrap";
import "../static/login.css";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }

  componentDidMount() {
    /* axios
      .post("http://localhost:5000/api/login", {
        email: this.state.email,
        password: this.state.password
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  } */
    axios({
      method: "post",
      url: "http://localhost:5000/api/login",
      data: {
        username: this.state.email,
        password: this.state.password
      }
    });
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
  };

  render() {
    return (
      <Container className="Login col-md-4 col-sm-12">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <FormLabel>Email</FormLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password</FormLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Container className="col-md-5 col-sm-12">
            <Button
              variant="danger"
              block
              bsSize="large"
              onClick={this.validateForm()}
              type="submit"
            >
              Login
            </Button>
          </Container>
        </form>
      </Container>
    );
  }
}

export default Login;
