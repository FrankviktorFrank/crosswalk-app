import React, { Component } from "react";
import Login from "./login";
import Navigationbar from "./navbar";
import LoggedIn from "./isLoggedIn";

let isLoggedIn = false;

class Main extends Component {
  constructor() {
    super();
    this.state = {
      projects: [
        {
          title: "Business",
          category: "Web design"
        },
        {
          title: "Social app",
          category: "Mobile dev"
        }
      ]
    };
  }

  //Hämta data från ifyllda fält i login, och fråga med hjälp av rest-api om inloggningsuppgifter finns/stämmer.
  /*  componentDidMount() {
    fetch("localhost:3001/rest-api/login");
  } */

  render() {
    return (
      <div>
        <Navigationbar />
        {/*  <LoggedIn projects={this.state.projects} /> */}
        {isLoggedIn ? <LoggedIn /> : <Login />}
      </div>
    );
  }
}
export default Main;
