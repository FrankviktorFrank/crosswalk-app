import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.css";
import Main from "./components/main";

ReactDOM.render(<Main />, document.getElementById("root"));

serviceWorker.unregister();
