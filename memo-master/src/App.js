import React, { Component } from 'react';

import './App.css';
import Menu from './Components/Menu';
import AITJumbotron from './Components/Jumbotron';
import RecentDocsCards from './Components/RecentDocsCards';
import Fastlane from './Components/Fastlane';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu />
        <AITJumbotron />
        <Fastlane />
        <RecentDocsCards />
      </div>
    );
  }
}

export default App;
