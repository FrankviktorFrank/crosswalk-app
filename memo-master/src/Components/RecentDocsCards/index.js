import React from 'react';
import { Row, Col } from 'reactstrap';
import Cards from './Card';
import './style.css';

const rows = [];

const cardData = [
    {
        title: 'Kallelse 1971',
        subtitle: 'Word-dokument',
        text: 'Dokument från Björklinge bygdegård',
    },
    {
        title: 'Gruppfoto',
        subtitle: 'Bild',
        text: 'Bild på föreningsmedlemmarna i konferenslokalen',
        imageUrl: 'https://www.yammer.com/api/v1/uploaded_files/136500047/version/138065140/preview/2018-06-02_10-21-03-210.jpg',
    },
    {
        title: 'Styrdokument 2017',
        subtitle: 'PDF-dokument',
        text: 'Styrande dokument för verksamhetsår 2017',
    },
    {
        title: 'Styrdokument 2016',
        subtitle: 'PDF-dokument',
        text: 'Styrande dokument för verksamhetsår 2016',
    },
    {
        title: 'Styrdokument 2015',
        subtitle: 'PDF-dokument',
        text: 'Styrande dokument för verksamhetsår 2015',
    },
]
for (var i = 0; i < cardData.length; i++) {
    rows.push(<Col sm="2" key={i}>
        <Cards data={cardData[i]}/>
    </Col>);
}

const RecentDocsCards = (props) => {
  return (
    <div className="paddingLeft">
        <h3>Senaste arkiverade dokumenten</h3>
        <Row>
        {rows}
        </Row>
    </div>
    
  );
};

export default RecentDocsCards;
