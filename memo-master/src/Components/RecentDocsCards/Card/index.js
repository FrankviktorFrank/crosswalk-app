import React from 'react';
import {
  Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button,
} from 'reactstrap';
import './style.css';

const Cards = (props) => {
  const data = props.data;
  const imageUrl = (data.imageUrl) ? data.imageUrl : 'https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180';
  return (
    <div>
      <Card>
        <CardImg top width="100%" src={imageUrl} alt="Card image cap" />
        <CardBody>
          <CardTitle>{data.title}</CardTitle>
          <CardSubtitle>{data.subtitle}</CardSubtitle>
          <CardText>{data.text}</CardText>
          <Button>Läs mer</Button>
        </CardBody>
      </Card>
    </div>
  );
};

export default Cards;
