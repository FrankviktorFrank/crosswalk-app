import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
import './style.css';

const AITJumbotron = (props) => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">Välkommen, Tolvan!</h1>
        <p className="lead">Här är startsidan för din nya arkiveringstjänst. Använd menyn för att navigera och följ gärna våran guidade tur nedan.</p>
        <hr className="my-2" />
        <p>Har du några frågor så kontakta oss på supporten så hjälper vi dig! </p>
        <p className="lead">
          <Button color="primary">Guidad tur</Button>
        </p>
      </Jumbotron>
    </div>
  );
};

export default AITJumbotron;
