import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge,
} from 'reactstrap';
import logo from './logo.png';
import './style.css';

class Menu extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    return (
      <div>
        <Navbar expand="md">
          <NavbarBrand href="/"><img src={logo} className="menu-logo" alt="logo" /></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/">Notiser <Badge color="secondary">4</Badge></NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/addDocument">Lägg till dokument</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/listDocument">Sök i ditt arkiv</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                Tolvan Tolvsson
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Inställningar
                  </DropdownItem>
                  <DropdownItem>
                    Support
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Logga ut
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
export default Menu;
