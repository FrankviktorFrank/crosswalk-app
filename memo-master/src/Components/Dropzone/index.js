import React from "react";
import Dropzone from "react-dropzone";
import "./style.css";

class DropZone extends React.Component {
  constructor() {
    super();
    this.state = { files: [] };
  }

  onDrop(files) {
    this.setState({
      files
    });
  }

  render() {
    return (
      <section>
        <div className="dropzone">
          <Dropzone
            className="dropzoneBox"
            accept="image/*,.docx,.pdf"
            onDrop={this.onDrop.bind(this)}
          >
            <p>
              Dra filen till den här rutan eller klicka för att välja fil.
              Endast bildfiler samt .docx och .pdf-filer godkänns
            </p>
          </Dropzone>
        </div>
        <aside>
          <p>Uppladdade filer</p>
          <ul>
            {this.state.files.map(f => (
              <li key={f.name}>
                {f.name} - {f.size} bytes
              </li>
            ))}
          </ul>
        </aside>
      </section>
    );
  }
}

export default DropZone;
