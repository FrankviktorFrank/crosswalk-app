import React from 'react';
import { Row, Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import './style.css';
import DropZone from '../Dropzone/';

const Fastlane = (props) => {
  return (
    <div className="fastlaneContainer">
        <Row>
          <Col sm={{ size: 10, offset: 1 }}>
            <h3>Snabbarkivering</h3>
            <hr className="my-2" />
          </Col>
        </Row>
        <Row>
          <Col sm={{ size: 10, offset: 1 }}>
          <Form>
            <FormGroup row>
              <Label for="title" sm={2}>Titel</Label>
              <Col sm={10}>
                <Input type="text" name="title" id="title" placeholder="Styrdokument 2019" />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="author" sm={2}>Författare/Skapare</Label>
              <Col sm={10}>
                <Input type="text" name="author" id="author" placeholder="Personen som skapat eller författat dokumentet" />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="owner" sm={2}>Ägare</Label>
              <Col sm={10}>
                <Input type="text" name="owner" id="owner" placeholder="T.ex. en förening eller privatperson" />
              </Col>
            </FormGroup>

            <FormGroup tag="fieldset" row>
              <legend className="col-form-label col-sm-2">Övriga uppgifter</legend>
              <Col sm={{ size: 10, offset: 2 }}>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" id="checkbox2" />{' '}
                      Dokumentet har personuppgifter i sig
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" id="checkbox2" />{' '}
                      Dokumentet får visas publikt
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" id="checkbox2" />{' '}
                      Dokumentet är en offentlighandling
                  </Label>
                </FormGroup>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="description" sm={2}>Beskrivning</Label>
              <Col sm={10}>
                <Input type="textarea" name="description" id="description" />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="regBy" sm={2}>Registrerad av</Label>
              <Col sm={10}>
                <Input type="text" name="regBy" id="regBy" value="Tolvan Tolvsson" disabled/>
              </Col>
              <Label for="regDate" sm={2}>Registrerad den:</Label>
              <Col sm={10}>
                <Input type="text" name="regDate" id="regDate" value="2018-08-30" disabled/>
              </Col>
            </FormGroup>

            
            <FormGroup row>
              <Label for="dropZone" sm={2}>Filuppladning</Label>
              <Col sm={10}>
                <DropZone />
              </Col>
            </FormGroup>
            
            <FormGroup check row>
              <Col sm={12}>
                <Button>Arkivera</Button>
                <hr className="my-2" />
              </Col>
            </FormGroup>
          </Form>
          </Col>
          
        </Row>
    </div>
  );
};

export default Fastlane;
