# memo
>Following instructions are written assuming you are using Ubuntu 16.04 or [Windows with subsystem Ubuntu](https://docs.microsoft.com/en-us/windows/wsl/install-win10). 

### Screenshot

The latest screenshot of Memo (2018-08-28)

![Screenshot](screenshots/2018-08-28.png)
___

### Dependencies
* [node.js](https://nodejs.org/) > 8.11
* [git](https://git-scm.com/downloads) > 2.7.4 

### Installation
Clone the project to your computer and execute Nodes installation.

```
cd /path/to/parent/folder/
git clone git@gitlab.com:arkivit/memo.git memo
```

### Running the application
First time you are running the application you have to run `npm install`

>If the installer says that its skipping fsevents its fine, its a module used for OS X. 

To have the latest version run `git pull` followed by `npm install`.

To start the server use `npm start`.

The application is now available at `http://localhost:3000`.
____

### Adding features to Memo

#### Feature-branch development
We are using strict [feature-branch development](https://nvie.com/posts/a-successful-git-branching-model/). If you are adding code to the project you need to follow these practises. 

>Hans did a quick fix so he did a commit in the master branch and pushed it. This made Greta angry because she had to do a merge fix and the new guy Bob started following Hans bad examples. 
>Don't be like Hans.

>Greta did a quick fix, started a branch named 'Fixing-menu-in-Edge' and did her fix. She then pushed it and merged the branch with Master. 
>Be like Greta.

#### ESLint
We will also be using ESLint, a tool that will help you find errors before they happen and also help you avoid pushing code that is not ready and cleaned up.

The project will be using Airbnbs approach to [Javascript](https://github.com/airbnb/javascript) and [React](https://github.com/airbnb/javascript/tree/master/react). 

`npm run lint` will execute ESLint in the src folder. 

### Documentation

#### REST-API

Daniel Westerling vote for using the [OpenAPI](https://swagger.io/specification/) standard when writing our API responses. 

This will allow for a safer and faster development, when we can assume how the response will look like. This will also simplify the way we write our API documentation.